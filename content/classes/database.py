class CategoriesTable:
    """Class made to represent the Categories table into the openfoodpy db and
    to interact with it"""

    def __init__(self, database):
        self.db = database
        self.db.query('USE openfoodpy')
        self.list_table = self._request_categories()

    def _request_categories(self):
        """request the categories into the table and return them as a list"""
        rows = self.db.query('SELECT * FROM Categories')
        list_table = []
        for row in rows:
            list_table.append((row.id, row.name))
        return list_table


class FoodTable:
    """Class made to represent the Food table into the openfoodpy db and
    to interact with it"""

    def __init__(self, database, category_id):
        self.db = database
        self.db.query('USE openfoodpy')
        self.category = category_id
        self.list_table = self._request_food()

    def _request_food(self):
        """Request food associated to the category the user choosed from the db
        and return the results as a list containing tupple with id/name/brand and
        nutriscore"""
        list_table = []
        rows = self.db.query(
            'SELECT DISTINCT * FROM Food '
            'INNER JOIN Union_food_cat '
            'ON Food.id = Union_food_cat.food_id '
            'INNER JOIN Categories '
            'ON Union_food_cat.cat_id = Categories.id '
            'WHERE Categories.id = {0} '
            'ORDER BY Food.id ASC'.format(self.category)
        )
        for row in rows:
            list_table.append((row.id, row.name, row.brand, row.nutriscore))
        return list_table


class SubstituteFoodTable:
    """Class made to represent the Substitute Table an aliment can have from the
     db and represent the results as a list containing tupple with info about
     the substitutes"""

    def __init__(self, database, category_id, nutriscore):
        self.db = database
        self.db.query('USE openfoodpy')
        self.category = category_id
        self.nutriscore = nutriscore
        self.list_table = self._request_food()

    def _request_food(self):
        list_table = []
        if self.nutriscore == "e" or "unknown":
            rows = self.db.query(
                'SELECT DISTINCT * FROM Food '
                'INNER JOIN Union_food_cat '
                'ON Food.id = Union_food_cat.food_id '
                'INNER JOIN Categories '
                'ON Union_food_cat.cat_id = Categories.id '
                'WHERE Categories.id = {0} '
                'AND (Food.nutriscore = "d" '
                'OR Food.nutriscore = "c" '
                'OR Food.nutriscore = "b" '
                'OR Food.nutriscore = "a") '
                'ORDER BY Food.id ASC'.format(self.category)
            )
        elif self.nutriscore == "d":
            rows = self.db.query(
                'SELECT DISTINCT * FROM Food '
                'INNER JOIN Union_food_cat '
                'ON Food.id = Union_food_cat.food_id '
                'INNER JOIN Categories '
                'ON Union_food_cat.cat_id = Categories.id '
                'WHERE Categories.id = {0} '
                'AND (Food.nutriscore = "c" '
                'OR Food.nutriscore = "b" '
                'OR Food.nutriscore = "a") '
                'ORDER BY Food.id ASC'.format(self.category)
            )
        elif self.nutriscore == "c":
            rows = self.db.query(
                'SELECT DISTINCT * FROM Food '
                'INNER JOIN Union_food_cat '
                'ON Food.id = Union_food_cat.food_id '
                'INNER JOIN Categories '
                'ON Union_food_cat.cat_id = Categories.id '
                'WHERE Categories.id = {0} '
                'AND (Food.nutriscore = "b" '
                'OR Food.nutriscore = "a") '
                'ORDER BY Food.id ASC'.format(self.category)
            )
        elif self.nutriscore == "b" or self.nutriscore == "a":
            rows = self.db.query(
                'SELECT DISTINCT * FROM Food '
                'INNER JOIN Union_food_cat '
                'ON Food.id = Union_food_cat.food_id '
                'INNER JOIN Categories '
                'ON Union_food_cat.cat_id = Categories.id '
                'WHERE Categories.id = {0} '
                'AND (Food.nutriscore = "a") '
                'ORDER BY Food.id ASC'.format(self.category)
            )
        for row in rows:
            list_table.append((row.id, row.name, row.brand))
        return list_table


class FoodRow:
    """Class who represent the row from the food table corresponding to the user
    choice"""

    def __init__(self, database, food_id):
        self.db = database
        self.db.query('USE openfoodpy')
        self.id = food_id
        self._request_food_data()

    def _request_food_data(self):
        result = self.db.query(
            "SELECT * FROM Food "
            "WHERE id = {0}".format(self.id)
        )
        self.name = result[0].name
        self.brand = result[0].brand
        self.nutriscore = result[0].nutriscore
        self.url = result[0].url


class UserTable:
    """Class made to represent all the interaction need with the User table in
    the db"""

    def __init__(self, database):
        self.db = database
        self.db.query("USE openfoodpy")

    def create_user(self, username, password_hash):
        """Create a new user into the table"""
        self.db.query(
            "INSERT INTO Users "
            "VALUES (NULL, '{0}', '{1}', FALSE)".format(username, password_hash)
        )

    def password_hash_request(self, username):
        """Return the password hash associated to the username"""
        rows = self.db.query(
            "SELECT hash FROM Users "
            "WHERE name = '{0}'".format(username)
        )
        return rows.first().hash

    def user_id_request(self, username):
        """Return the id associated to the username"""
        rows = self.db.query(
            "SELECT id FROM Users "
            "WHERE name = '{0}'".format(username)
        )
        return rows.first().id

    def block_user(self, user_id, username):
        """change the BOOL in the column blocked to FALSE"""
        self.db.query(
            "UPDATE Users "
            "SET id = {0}, name = '{1}', hash = NULL, blocked = TRUE "
            "WHERE id = {0}".format(
                user_id, username, hash
            )
        )

    def username_available(self, username):
        """Check if a username is available, return True if it is, False if
        it's not"""
        result = self.db.query(
            "SELECT EXISTS(SELECT 1 FROM Users WHERE name LIKE '{0}')".format(
                username)
        )
        for value in result.first().values():
            if value == 1:
                return False
            elif value == 0:
                return True

    def is_blocked(self, username):
        """Check if a user is blocked, return True if it is, False if it's
        not"""
        rows = self.db.query(
            "SELECT blocked FROM Users "
            "WHERE name = '{0}'".format(username)
        )
        if rows.first().blocked == 1:
            return True
        elif rows.first().blocked == 0:
            return False


class SavedFoodTable:
    """Class made to represent the SavedFood table in the db"""

    def __init__(self, database):
        self.db = database
        self.db.query("USE openfoodpy")

    def save_food(self, food_id, user_id):
        """Add food to the table when the user request it"""
        self.db.query(
            "INSERT INTO Saved_food "
            "VALUES ({0}, {1}) "
            "ON DUPLICATE KEY UPDATE food_id = food_id".format(
                food_id, user_id)
        )

    def retrieve_saved_food(self, user_id):
        """Return a list of all the saved food associated to the user id"""
        rows = self.db.query(
            "SELECT * FROM Saved_food WHERE user_id = {}".format(user_id)
        )
        saved_aliments = []

        for row in rows:
            result = self.db.query(
                "SELECT * FROM Food WHERE id = {}".format(row.food_id)
            )
            name = result[0].name
            brand = result[0].brand
            nutriscore = result[0].nutriscore
            url = result[0].url
            saved_aliments.append((row.food_id, name, brand, nutriscore, url))

        return saved_aliment
