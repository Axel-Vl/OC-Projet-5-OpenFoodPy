from os import system
from time import sleep
from passlib.hash import pbkdf2_sha256
from content.classes.database import UserTable


class User:
    """Manage users in the program"""

    def __init__(self, database):
        self.user_table = UserTable(database)
        self.username = None
        self.user_id = None

        # Flags
        self.verified = False
        self.can_be_verified = True
        self.attempt = 5

    def create_user(self):
        """Method to create a user and add it to the db"""
        self._reset_flags()
        username_created = False
        while username_created is False:
            username = input("Select a username: ")
            username = username.lower().strip()
            if not self.user_table.username_available(username):
                print("Already taken !")
            elif self.user_table.username_available(username):
                username_created = True

        password = input("Choose a password: ")
        password_hash = pbkdf2_sha256.hash(password)
        self.user_table.create_user(username, password_hash)
        self.username = username
        self.user_id = self.user_table.user_id_request(username)
        self.verified = True
        system('clear')
        print('You are connected as {}'.format(self.username))
        sleep(1)

    def connection(self):
        """Method who allow a user to connect himself to the db and gaining
        gaining acces to some functionality of the program"""
        flag = True
        temp_username = None
        self._reset_flags()
        while flag:
            temp_username = input("Insert your username: ")
            if not self.user_table.username_available(temp_username):
                self.username = temp_username
                self.user_id = self.user_table.user_id_request(self.username)
            elif self.user_table.username_available(temp_username):
                system('clear')
                print("Username doesn't exist, try again.")
                sleep(1)
                break

            if self.user_table.is_blocked(self.username):
                self.can_be_verified = False
                flag = False
                print("User blocked")

            while (self.verified is False) and (self.can_be_verified is True):
                password = input("Enter your password: ")
                self._verify_password(password)
                flag = False

    def _reset_flags(self):
        """Reset the flags to their initial states"""
        self.verified = False
        self.can_be_verified = True
        self.attempt = 5

    def _verify_password(self, password):
        """Self explanatory name, made to verify a user password"""
        password_hash = self.user_table.password_hash_request(self.username)

        if pbkdf2_sha256.verify(password, password_hash) is True:
            self.verified = True
        elif pbkdf2_sha256.verify(password, password_hash) is False:
            self.attempt -= 1
            print("Wrong password !")

        if self.attempt == 0:
            self.can_be_verified = False
            self._block_user()
            print("Too much wrong attempt !")
        elif not self.can_be_verified:
            print("user blocked, contact an admin")

    def _block_user(self):
        """Self explanatory name"""
        self.user_table.block_user(self.user_id, self.username)
