from os import system
from time import sleep
from content.classes.database import *
from content.classes.user import User


class Interface:
    """Main class of the program, represent the terminal based interface of
    openfoodpy"""

    def __init__(self, database):
        self.db = database
        self.db.query('USE openfoodpy')
        self.user = User(database)
        self.saved_food_table = SavedFoodTable(database)

        # Interface flags
        self.running = False
        self.current_menu = None
        self.menu_objects_list = []  # Stock previous menu objects

    def run(self):
        """main loop for the interface"""
        self.running = True
        self._create_start_menu()

        while self.running is True:
            system("clear")
            self.states_print()
            self.current_menu.display()
            self.commands_print()
            self.user_action()
        print("See you later !")

    def user_action(self):
        """Wait for an user input and choose the appropriate method based on the
        input"""
        user_input = self._user_input_filter()

        if user_input == "n":
            self._next_menu_page()
        elif user_input == "p":
            self._previous_menu_page()
        elif user_input == "r":
            self._return_to_previous_menu()
        elif user_input == "m":
            self._create_start_menu()
        elif user_input == "s":
            self._save_user_choice()
        elif user_input == "q":
            self.running = False
        elif user_input in range(0, 10):
            self._user_selection(user_input)

    def states_print(self):
        """Print at each begining of the main loop if the user is connected or
        not, and under wich username"""
        if self.user.verified:
            print("Connected as {0}".format(self.user.username))
        elif not self.user.verified:
            print("Not connected")

    def commands_print(self):
        """Print the commands available to the user based on the menu of the
        program he is currently browsing"""
        if type(self.current_menu) is StartMenu:
            commands = """----------
Commands :
(0/1: Selection)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is CategoriesMenu:
            commands = """----------
Commands :
(0 ... 999: Selection)
(p/n: previous page / next page)
(r: return to the previous menu)
(m: return to the main menu)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is FoodMenu:
            commands = """----------
Commands :
(0 ... 999: Selection)
(p/n: previous page / next page)
(r: return to the previous menu)
(m: return to the main menu)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is FoodSubstituteMenu:
            commands = """----------
Commands :
(0 ... 999: Selection)
(p/n: previous page / next page)
(r: return to the previous menu)
(m: return to the main menu)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is FoodInfoMenu:
            commands = """----------
Commands :
(r: return to the previous menu)
(m: return to the main menu)
(s: save your selection)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is UserMenu:
            commands = """----------
Commands :
(0/1/2: Selection)
(r: return to the previous menu)
(m: return to the main menu)
(q: quit OpenFoodPy)
----------"""
            print(commands)

        elif type(self.current_menu) is SavedFoodMenu:
            commands = """----------
Commands :
(0 ... 999: Selection)
(p/n: previous page / next page)
(r: return to the previous menu)
(m: return to the main menu)
(q: quit OpenFoodPy)
----------"""
            print(commands)

    # User input filter methods
    def _user_input_filter(self):
        """Filter the user input based on the menu he is currently browsing"""
        flag = False
        while flag is False:
            user_input = input("Type your command : ")
            try:
                user_input = int(user_input)
                user_input %= 10
            except ValueError:
                pass

            if (type(self.current_menu) is StartMenu) and (
                    self._start_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is CategoriesMenu) and (
                    self._categories_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is FoodMenu) and (
                    self._food_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is FoodSubstituteMenu) and (
                    self._food_substitute_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is FoodInfoMenu) and (
                    self._food_info_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is UserMenu) and (
                    self._user_menu_filter(user_input)):
                flag = True
                return user_input

            elif (type(self.current_menu) is SavedFoodMenu) and (
                    self._saved_food_menu_filter(user_input)):
                flag = True
                return user_input

    @staticmethod
    def _start_menu_filter(user_input):
        start_menu_inputs = (0, 1, "q")
        if user_input not in start_menu_inputs:
            print("Wrong input !")
            return False
        elif user_input in start_menu_inputs:
            return True

    @staticmethod
    def _categories_menu_filter(user_input):
        categories_menu_inputs = ("n", "p", "r", "q", "m")
        if (user_input not in categories_menu_inputs) and (
                type(user_input) is not int):
            print("Wrong input !")
            return False
        elif (user_input in categories_menu_inputs) or (
                type(user_input) is int):
            return True

    @staticmethod
    def _food_menu_filter(user_input):
        food_menu_inputs = ("n", "p", "r", "q", "m")
        if (user_input not in food_menu_inputs) and (
                type(user_input) is not int):
            print("Wrong input !")
            return False
        elif (user_input in food_menu_inputs) or (
                type(user_input) is int):
            return True

    @staticmethod
    def _food_substitute_menu_filter(user_input):
        food_substitute_menu_inputs = ("n", "p", "r", "q", "m")
        if (user_input not in food_substitute_menu_inputs) and (
                type(user_input) is not int):
            print("Wrong input !")
            return False
        elif (user_input in food_substitute_menu_inputs) or (
                type(user_input) is int):
            return True

    @staticmethod
    def _food_info_menu_filter(user_input):
        food_info_menu_inputs = ("r", "s", "q", "m")
        if user_input not in food_info_menu_inputs:
            print("Wrong input !")
            return False
        elif user_input in food_info_menu_inputs:
            return True

    @staticmethod
    def _user_menu_filter(user_input):
        user_menu_inputs = (0, 1, 2, "r", "q", "m")
        if user_input not in user_menu_inputs:
            print("Wrong input!")
            return False
        elif user_input in user_menu_inputs:
            return True

    @staticmethod
    def _saved_food_menu_filter(user_input):
        saved_food_menu_inputs = ("n", "p", "r", "q", "m")
        if (user_input not in saved_food_menu_inputs) and (
                type(user_input) is not int):
            print("Wrong input!")
            return False
        elif (user_input in saved_food_menu_inputs) or (
                type(user_input) is int):
            return True

    # User choices methods
    def _user_selection(self, item_number):
        """Choose a method to execute based on the menu the user is currently
        browsing and his input"""
        if type(self.current_menu) is StartMenu:
            if item_number == 0:
                self._create_categories_menu()
            elif item_number == 1:
                self._create_user_menu()

        elif type(self.current_menu) is UserMenu:
            if item_number == 0:
                self.user.connection()
                self._create_start_menu()
            elif item_number == 1:
                self.user.create_user()
            elif (item_number == 2) and self.user.verified:
                self._create_saved_food_menu()
            elif (item_number == 2) and not self.user.verified:
                print("You are not connected !!!! Connect first !!!!")
                sleep(1)

        elif type(self.current_menu) is CategoriesMenu:
            cat = self.current_menu.temp_items_list[item_number]
            self._create_food_menu(cat)

        elif type(self.current_menu) is FoodMenu:
            cat_id = self.current_menu.cat_id
            nutriscore = self.current_menu.temp_items_list[item_number][3]
            self._create_food_substitute_menu(cat_id,
                                              nutriscore)

        elif type(self.current_menu) is FoodSubstituteMenu:
            food_id = self.current_menu.temp_items_list[item_number][0]
            self._create_food_info_menu(food_id)

        elif type(self.current_menu) is SavedFoodMenu:
            food_id = self.current_menu.temp_items_list[item_number][0]
            self._create_food_info_menu(food_id)

    def _next_menu_page(self):
        """Change the page of the current menu"""
        if (self.current_menu.page + 1) * 10 < self.current_menu.count:
            self.current_menu.page += 1
        elif (self.current_menu.page + 1) * 10 >= self.current_menu.count:
            system('clear')
            print("\nLast page reached\n")
            sleep(1)

    def _previous_menu_page(self):
        """Change the page of the current menu"""
        if self.current_menu.page == 0:
            system('clear')
            print("\nFirst page reached\n")
            sleep(1)
        elif self.current_menu.page > 0:
            self.current_menu.page -= 1

    def _return_to_previous_menu(self):
        """Return to the previous menu browsed"""
        self.current_menu = self.menu_objects_list.pop(-1)

    def _save_user_choice(self):
        """Save the food the user is browsing to the db"""
        if self.user.verified:
            self.saved_food_table.save_food(self.current_menu.food_id,
                                            self.user.user_id)
            system('clear')
            print("Saved !")
            sleep(1)
        elif not self.user.verified:
            system('clear')
            print("You are not logged in !")
            sleep(1)

    # Menu creation methods
    def _create_start_menu(self):
        """Create a StartMenu object and set it to the self.current_menu
        variable"""
        start_menu = StartMenu()
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = start_menu

    def _create_categories_menu(self):
        """Create a CategoriesMenu object and set it to the self.current_menu
        variable"""
        categories_menu = CategoriesMenu(self.db)
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = categories_menu

    def _create_food_menu(self, cat_id):
        """Create a FoodMenu object and set it to the self.current_menu
        variable"""
        food_menu = FoodMenu(self.db, cat_id)
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = food_menu

    def _create_food_substitute_menu(self, cat_id, nutriscore):
        """Create a FoodSubstituteMenu object and set it to the
        self.current_menu variable"""
        food_substitute_menu = FoodSubstituteMenu(self.db, cat_id,
                                                  nutriscore)
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = food_substitute_menu

    def _create_food_info_menu(self, food_id):
        """Create a FoodInfoMenu object and set it to the
        self.current_menu variable"""
        food_info_menu = FoodInfoMenu(self.db, food_id)
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = food_info_menu

    def _create_user_menu(self):
        """Create a UserMenu object and set it to the
        self.current_menu variable"""
        user_menu = UserMenu()
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = user_menu

    def _create_saved_food_menu(self):
        """Create a SavedFoodMenu object and set it to the
        self.current_menu variable"""
        table = self.saved_food_table.retrieve_saved_food(
            self.user.user_id)
        saved_food_menu = SavedFoodMenu(table)
        self.menu_objects_list.append(self.current_menu)
        self.current_menu = saved_food_menu


class StartMenu:
    """First menu the user will encounter when launching the program"""

    @staticmethod
    def display():
        text = """
----OpenFoodPy----
        
0/ Browse Aliments 
1/ User connection / Create user / Saved food
"""
        print(text)


class CategoriesMenu:
    """ Class made to represent categories menu inside the interface """

    def __init__(self, db):
        self.table = CategoriesTable(db).list_table
        self.count = len(self.table)
        self.page = 0
        self.temp_items_list = []

    def display(self):
        """Method who print categories 10 by 10 in the terminal"""
        self._refresh_temp_items_list()
        x = self.page * 10

        # Pretty printing for the terminal
        if self.page * 10 <= (self.count - (self.count % 10)):
            self._pretty_print_list(x)

        elif self.page * 10 > (self.count - (self.count % 10)):
            # give x a new value to print the last 10 items of the list
            x = (self.page * 10) - (10 - (self.count % 10)) - 1
            self._pretty_print_list(x)

    def _pretty_print_list(self, x):
        print("----------\nCategories\nPage: {0}\n----------".format(self.page))
        for item in range(x, x + 10):
            try:
                text = str(str(item) + '/: ' + str(self.table[item][1]))
                print(text)
                self.temp_items_list.append(self.table[item][0])
            except IndexError:
                text = str("XXX/ Error")

    def _refresh_temp_items_list(self):
        self.temp_item_ids_list = []


class FoodMenu:
    """Class made to represent the food associated to the categorie choosed by
    the user"""

    def __init__(self, db, cat_id):
        self.table = FoodTable(db, cat_id).list_table
        self.count = len(self.table)
        self.cat_id = cat_id
        self.page = 0
        self.temp_items_list = []

    def display(self):
        """Method who print categories 10 by 10 in the terminal"""
        self._refresh_temp_items_list()
        x = self.page * 10

        # Pretty printing for the terminal
        if self.page * 10 <= (self.count - (self.count % 10)):
            self._pretty_print_list(x)

        elif self.page * 10 > (self.count - (self.count % 10)):
            # give x a new value to print the last 10 items of the list
            x = (self.page * 10) - (10 - (self.count % 10)) - 1
            self._pretty_print_list(x)

    def _pretty_print_list(self, x):
        print("----------\nFood\nPage: {0}\n----------".format(self.page))
        for item in range(x, x + 10):
            try:
                text = str(str(item) + '/ Name: ' + str(self.table[item][1]) +
                           " / Brand: " + str(self.table[item][2]))
                print(text)
                self.temp_items_list.append(self.table[item])
            except IndexError:
                text = str("XXX/ Error")

    def _refresh_temp_items_list(self):
        self.temp_items_list = []


class FoodSubstituteMenu:
    """Class made to represent the substitute associated to the food the user
    choosed"""

    def __init__(self, db, cat_id, nutriscore):
        self.table = SubstituteFoodTable(db, cat_id, nutriscore).list_table
        self.count = len(self.table)
        self.page = 0
        self.temp_items_list = []

    def display(self):
        """Method who print categories 10 by 10 in the terminal"""
        self._refresh_temp_items_list()
        x = self.page * 10

        # Pretty printing for the terminal
        if self.page * 10 <= (self.count - (self.count % 10)):
            self._pretty_print_list(x)

        elif self.page * 10 > (self.count - (self.count % 10)):
            # give x a new value to print the last 10 items of the list
            x = (self.page * 10) - (10 - (self.count % 10)) - 1
            self._pretty_print_list(x)

    def _pretty_print_list(self, x):
        print("----------\nSubstitute\nPage: {0}\n----------".format(self.page))

        for item in range(x, x + 10):
            try:
                text = str(
                    str(item) + '/ Name: ' + str(self.table[item][1]) +
                    " / Brand: " + str(self.table[item][2]))
                print(text)
                self.temp_items_list.append(self.table[item])
            except IndexError:
                text = str("XXX/ Error")

    def _refresh_temp_items_list(self):
        self.temp_items_list = []


class FoodInfoMenu:
    """Class made to represent the Information associated to a food choosed by
    the user"""

    def __init__(self, db, food_id):
        food = FoodRow(db, food_id)
        self.food_id = food.id
        self.food_name = food.name
        self.food_brand = food.brand
        self.food_nutriscore = food.nutriscore
        self.food_url = food.url

    def display(self):
        # Pretty printing for the terminal
        print("----------")
        text = """Food id = {0}
            \nFood name = {1}
            \nFood brand = {2}
            \nFood nutriscore = {3}
            \nFood url = {4}""".format(self.food_id,
                                       self.food_name,
                                       self.food_brand,
                                       self.food_nutriscore,
                                       self.food_url)
        print(text)


class UserMenu:
    """Class made to represent the UserMenu, will only display the choice the
    user can make"""

    @staticmethod
    def display():
        text = """
What would you like to do ?

0/ Connection
1/ Create account
2/ Display saved food"""
        print(text)


class SavedFoodMenu:
    """Class made to represent the Saved food the user can access from the db"""

    def __init__(self, saved_aliments_list):
        self.table = saved_aliments_list
        self.count = len(self.table)
        self.page = 0
        self.temp_items_list = []

    def display(self):
        """Method who print categories 10 by 10 in the terminal"""
        self._refresh_temp_items_list()
        x = self.page * 10

        # Pretty printing for the terminal
        if self.page * 10 <= (self.count - (self.count % 10)):
            self._pretty_print_list(x)

        elif self.page * 10 > (self.count - (self.count % 10)):
            # give x a new value to print the last 10 items of the list
            x = (self.page * 10) - (10 - (self.count % 10)) - 1
            self._pretty_print_list(x)

    def _pretty_print_list(self, x):
        print("----------\nSaved Food\nPage: {0}\n----------".format(
            self.page))

        for item in range(x, x + 10):
            try:
                text = str(str(item) + '/ Name: ' + str(self.table[item][1]) +
                           " / Brand: " + str(self.table[item][2]))
                print(text)
                self.temp_items_list.append(self.table[item])
            except IndexError:
                text = str("XXX/ Error")

    def _refresh_temp_items_list(self):
        self.temp_items_list = []
