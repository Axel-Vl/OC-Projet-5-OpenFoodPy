"""Script to create the database needed for the OpenFoodPy program, will fill it
with data queried from the OpenFoodFact API
MariaDB is used here"""

import requests
import records
import sqlalchemy
import sys
import os

# initialise the db to use and create a bug log
db = records.Database('mysql+pymysql://root:axelaxel@localhost')
try:
    os.remove(
        "/Users/axel/Documents/Programmation/OpenClassRooms/"
        "projet-5/content/logs/bug_log_database_construction.txt")
except FileNotFoundError:
    pass
bug_log = open(
    "/Users/axel/Documents/Programmation/OpenClassRooms/"
    "projet-5/content/logs/bug_log_database_construction.txt",
    "w")


def db_construction(number_of_cat_to_work_with):
    """Main function for the script, you have to pass how many categories you
    want to work with, this will request 1000 elements by 1000 elements to the
    OpenFoodFact API, execution time might be long !
    (approx. 5mn for 10 cats)"""
    db_connection()
    create_tables()
    get_categories(number_of_cat_to_work_with)
    get_food(number_of_cat_to_work_with)


def db_connection():
    """Connect to the openfoodpy db, create it if it doesn't exist"""
    try:
        db.query('USE openfoodpy')

    except sqlalchemy.exc.InternalError:
        db.query("CREATE DATABASE openfoodpy CHARACTER SET 'utf8'")
        db.query('USE openfoodpy')
    print("Connected to OpenFoodPY db\n---")


def create_tables():
    """Create the tables needed for our program to work"""
    try:

        db.query(
            'CREATE TABLE Food('
            'id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,'
            'name MEDIUMTEXT NOT NULL,'
            'brand MEDIUMTEXT,'
            'nutriscore VARCHAR(10),'
            'url TEXT)'
        )
        print('Table Aliments created\n---')
    except sqlalchemy.exc.InternalError:
        print("Table 'food' already exists\n---")

    try:
        db.query(
            'CREATE TABLE Categories('
            'id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,'
            'name MEDIUMTEXT NOT NULL,'
            'url TEXT NOT NULL,'
            'product_count MEDIUMINT NOT NULL)'
        )
        print('Table Categories created\n---')
    except sqlalchemy.exc.InternalError:
        print("Table 'Categories' already exists\n---")

    try:
        db.query(
            'CREATE TABLE Union_food_cat('
            'food_id INT UNSIGNED ,'
            'cat_id INT UNSIGNED )'
        )
        print('Table Union_alim_cat created\n---')
    except sqlalchemy.exc.InternalError:
        print("Table 'Union_food_cat' already exists\n---")

    try:
        db.query(
            'CREATE TABLE Users('
            'id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,'
            'name MEDIUMTEXT NOT NULL UNIQUE,'
            'hash VARCHAR(87),'
            'blocked BOOL)'
        )
    except sqlalchemy.exc.InternalError:
        print("Table 'Users' already exists\n---")

    try:
        db.query(
            'CREATE TABLE Saved_food('
            'food_id INT UNSIGNED ,'
            'user_id INT UNSIGNED)'
        )
        db.query(
            "ALTER TABLE Saved_food ADD PRIMARY KEY (food_id, user_id)"
        )
    except sqlalchemy.exc.InternalError:
        print("Table 'Saved_food' already exists\n---")


def get_categories(number_of_cat_to_work_with):
    """Retrieve the categories from the open food facts API and insert them
    into the Categories table of the db"""

    request_link = "https://fr.openfoodfacts.org/categories&json=1"
    request = requests.get(request_link)
    print('OpenFoodFacts categories json requested\n---')
    request_dict = request.json()  # convert the json file to a python dict
    count = request_dict["count"]  # number of categories in O.F.F.

    # Check if the amount of cat requested is higher or lower from the amount
    # of cat existing in the OpenFoodFact db
    if number_of_cat_to_work_with < count:
        for category in range(0, number_of_cat_to_work_with):
            raw_name = str(request_dict["tags"][category]["name"])
            name = string_cleaner(raw_name)
            url = str(request_dict["tags"][category]["url"])
            product_amount = int(request_dict["tags"][category]["products"])
            mysql_insert_categories_query(name, url, product_amount)

    elif number_of_cat_to_work_with >= count:
        for category in range(0, count):
            raw_name = str(request_dict["tags"][category]["name"])
            name = string_cleaner(raw_name)
            url = str(request_dict["tags"][category]["url"])
            product_amount = int(request_dict["tags"][category]["products"])
            mysql_insert_categories_query(name, url, product_amount)

    print('All categories inserted into db\n---\n')


def mysql_insert_categories_query(name, url, product_amount):
    """MySql query to insert the categories into the db (row by row)"""
    try:
        db.query(
            "INSERT INTO Categories "
            "VALUES (NULL, '{0}', '{1}', {2})".format(name, url, product_amount)
        )
    except UnicodeEncodeError:
        bug = sys.exc_info()[1]
        bug_log.write(
            "Error, cat: {0} \n    {1}\n-----------\n".format(name, bug))
        pass
    except sqlalchemy.exc.ProgrammingError:
        bug = sys.exc_info()[1]
        bug_log.write(
            "ProgrammingError, cat: {0} \n    {1}\n----------\n".format(name,
                                                                        bug))


def get_food(number_of_cat_to_work_with):
    """Request 1000 food by categories to the O.F.F. API and insert them into
    the db"""
    x = 0 # flag variable to break the loop if needed
    categories = db.query("SELECT * FROM Categories")
    for categorie in categories:
        request_dict = request_food_to_api(categorie.url)
        insert_food_into_db(request_dict, x + 1)
        if x == number_of_cat_to_work_with:
            break
        x += 1


def request_food_to_api(category_url):
    """request data from the O.F.F. API and convert it to a python dict"""
    category_split = category_url.split("/")
    category = category_split[-1]
    request_link = "https://fr.openfoodfacts.org/cgi/search.pl?" \
                   "action=process&" \
                   "tagtype_0=categories&" \
                   "tag_contains_0=contains&" \
                   "tag_0={0}&" \
                   "page_size=1000&" \
                   "json=1".format(category)
    request = requests.get(request_link)
    print('Request: ' + request_link + '\n---')
    request_dict = request.json()
    return request_dict


def insert_food_into_db(request_dict, current_cat_id):
    """Insert the food into the database, clean their name/brand and nutrition
    grades a.k.a. nutriscore"""
    count = request_dict["count"]
    if count > 1000:
        count = 1000
    for aliment in range(0, count):
        try:
            raw_food_name = request_dict["products"][aliment]["product_name"]
            food_name = string_cleaner(raw_food_name)
        except KeyError:
            pass
        except IndexError:
            pass
        try:
            raw_food_brand = request_dict["products"][aliment]["brands"]
            food_brand = string_cleaner(raw_food_brand)
        except KeyError:
            pass
        except IndexError:
            pass
        try:
            food_nutriscore = request_dict["products"][aliment][
                'nutrition_grades']
        except KeyError:
            food_nutriscore = 'unknown'
        try:
            food_url = request_dict["products"][aliment]["url"]
        except KeyError:
            food_url = 'unknown'
        try:
            food_categories = request_dict["products"][aliment]["categories"]
            food_categories = food_categories.split(',')
        except KeyError:
            food_categories = []

        # inserting the information into the db
        mysql_insert_food_query(food_name, food_brand, food_nutriscore, food_url)
        mysql_union_table_query(food_name, food_categories, current_cat_id)


def mysql_insert_food_query(food_name, food_brand, food_nutriscore, food_url):
    """MySQL request to insert the food into the db"""
    try:
        db.query(
            "INSERT INTO Food "
            "VALUES (NULL, '{0}', '{1}', '{2}', '{3}')".format(
                food_name,
                food_brand,
                food_nutriscore,
                food_url)
        )
    except UnicodeEncodeError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n".format(bug))
        pass


def mysql_union_table_query(food_name, food_categories, current_cat_id):
    """insert the aliment id and the different categories id associated to him
        into the union table"""
    try:
        food_id = db.query(
            "SELECT * FROM Food WHERE name = '{0}'".format(food_name)
        )
        f_id = food_id[0].id
    except IndexError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n".format(bug))
    except UnicodeEncodeError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n".format(bug))

    try:
        db.query(
            "INSERT INTO Union_food_cat "
            "VALUES ({0}, {1})".format(f_id, current_cat_id)
        )
    except IndexError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n".format(bug))
        pass
    except UnicodeEncodeError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n".format(bug))
        pass
    except UnboundLocalError:
        bug = sys.exc_info()
        bug_log.write("ERROR: {0} \n{1}\n".format(bug, food_name))

    for category in food_categories:
        try:
            category_id = db.query(
                'SELECT * FROM Categories WHERE name = "{0}"'.format(category)
            )
            cat_id = category_id[0].id

            db.query(
                "INSERT INTO Union_food_cat "
                "VALUES ({0}, {1})".format(f_id, cat_id)
            )
        except IndexError:
            bug = sys.exc_info()
            bug_log.write("ERROR: {0} \n".format(bug))
            pass
        except UnicodeEncodeError:
            bug = sys.exc_info()
            bug_log.write("ERROR: {0} \n".format(bug))
            pass
        except UnboundLocalError:
            bug = sys.exc_info()
            bug_log.write("ERROR: {0} \n{1}\n".format(bug, food_name))


def string_cleaner(name):
    """function created to clean the data we have to insert into the db into
    something with the same pattern (avoid MySQL ERROR is the
    primary goal here)"""
    name_bis = name
    name_bis = name_bis.replace('"', ' ')
    name_bis = name_bis.replace("'", " ")
    name_bis = name_bis.replace("-", " ")
    name_bis = name_bis.replace(",", " ")
    name_bis = name_bis.strip()
    return name_bis
