OpenFoodPy
----------


### Introduction

This software is based on the OpenFoodFacts database.
It allow his user to search different kind of food in the database and the 
software will propose to the user an alternative with better attributes for 
the health.

The user will also be capabable to connect himself to the application and save food he want to retrieve later.

#### I. Search food


Once the software is running, 2 choices will be available:
* 0/ Browse Aliments
* 1/ User connection / Create user / Saved food


The software is now waiting an input from the user, a number corresponding 
to one of the choices above.
In our case it's the number 0.

 
The software will now display a list of different categories of food and is 
waiting for an input.

 
Once the category is selected, a list with food will be displayed and the
software is waiting for an input from the user.
 
Then, the software will show a list with different alternatives to the food 
we choosed in the list with better 'nutriscore' (classification method used in Openfoodfacts)

Finnaly, the program will show details about the aliment we picked and
will give a link to the Openfoodfacts page to the user
 
 
#### II. User connection / Create user / Saved food

OpenFoodPy allow the user to save food to check it later.

To do it, the user need to be connected in the first time and he will
just have to select the Save command when he is in the details menu of an aliment

Then when he want to retrieve all his saved aliments, he'll just have to select the Saved food menu
available from the main menu.



